![](http://flask-restplus.readthedocs.io/en/stable/_static/logo-512.png)

### TODO-LIST API (Python)

----------
- 개발환경
    - OS : Mac OS
    - IDE : Pycharm
    - Python :
        - version : 3.6
        - Flask : Flask-RESTPlus
    - Database : Mysql
    - Documentation : /documentation

- 인증
    - JWT
        - IN : Header
        - Key : X-TodoList-Authorization
        - AccessToken : expire 1days
        - RefreshToken : expire 7days        
        - Route : /auth/login