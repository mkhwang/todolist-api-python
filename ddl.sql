CREATE TABLE user
(
    idx BIGINT PRIMARY KEY AUTO_INCREMENT,
    user_id VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    name varchar(20) NOT NULL,
    role ENUM('ROLE_ADMIN', 'ROLE_USER') DEFAULT 'ROLE_USER',
    created_at DATETIME DEFAULT now(),
    updated_at DATETIME DEFAULT NULL,
    login_at DATETIME DEFAULT NULL,
    logout_at DATETIME DEFAULT NULL,
    deleted_at DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE UNIQUE INDEX user_id_uindex ON user (user_id);

CREATE TABLE task
(
    idx BIGINT PRIMARY KEY AUTO_INCREMENT,
    user_idx BIGINT,
    title VARCHAR(100) NOT NULL,
    content TEXT,
    is_finished BOOLEAN DEFAULT FALSE,
    created_at DATETIME DEFAULT now(),
    updated_at DATETIME DEFAULT NULL,
    deleted_at DATETIME DEFAULT NULL,
    started_at DATETIME NOT NULL,
    deadline_at DATETIME NOT NULL,
    FOREIGN KEY (`user_idx`) REFERENCES `user` (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


show PROCESSLIST ;