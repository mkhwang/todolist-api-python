from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_restplus import Namespace, Resource, reqparse, fields
from jwt import InvalidTokenError

from app.api.core.response import CommonResponse, common_response_field
from app.domain.user.user import UserType
from app.service.auth_service import AuthService
from app.service.task_service import TaskService

ns_admin = Namespace('관리자', description='관리자 기능')

auth_service = AuthService()
task_service = TaskService()

modify_user_role_fields = ns_admin.model('UserRoleMode', {
    'userIdx': fields.Integer(required=True, location='path'),
    'role': fields.String(enum=UserType._member_names_, location='json')
})

me_modify_parser = reqparse.RequestParser(bundle_errors=True)
me_modify_parser.add_argument('role', type=UserType, choices=UserType._member_names_, trim=True, required=True,
                              location='args')

user_model = ns_admin.model('User', {
    'idx': fields.Integer,
    'userId': fields.String(attribute='user_id'),
    'name': fields.String,
    'password': fields.String,
    'role': fields.String(enum=UserType._member_names_),
    'createdAt': fields.String(attribute='created_at'),
    'loginAt': fields.String(attribute='login_at')
})

admin_user_response = ns_admin.clone('AdminUsersResponse', common_response_field, {
    'data': fields.List(fields.Nested(user_model))
})


class AdminResponse(CommonResponse):
    def __init__(self, data):
        super().__init__()
        self.data = data


def admin_role_filter(message):
    if 'role' not in message:
        raise InvalidTokenError('Invalid Token')
    if not 'ROLE_ADMIN'.__eq__(message['role']):
        raise NoAuthorizationError('You have no authority about this resource')
    return True


@ns_admin.route('/users')
class AdminUser(Resource):
    @jwt_required
    @ns_admin.doc(security='JWT', description='모든 사용자 정보 보기')
    @ns_admin.marshal_with(admin_user_response)
    def get(self):
        """
        모든 사용자 가져오기
        """
        identity = get_jwt_identity()
        admin_role_filter(identity)
        return AdminResponse(auth_service.find_all_users()), 200


@ns_admin.route('/users/<int:userIdx>')
class AdminUserModify(Resource):
    @jwt_required
    @ns_admin.doc(security='JWT', description='사용자 권한 수정 "ROLE_ADMIN" or "ROLE_USER"')
    @ns_admin.expect(me_modify_parser)
    def put(self):
        """
        사용자 수정(권한변경)
        """
        identity = get_jwt_identity()
        admin_role_filter(identity)
        return None, 204


@ns_admin.route('/tasks')
class AdminTask(Resource):
    @jwt_required
    @ns_admin.doc(security='JWT', description='모든 업무 정보 보기')
    def get(self):
        """
        모든 업무 보기
        """
        identity = get_jwt_identity()
        admin_role_filter(identity)
        return None, 200
