from flask_restplus import Model, fields

common_response_field = Model('CommonResponse', {
    'result': fields.Boolean,
    'message': fields.String,
})


class CommonResponse:
    result = None
    message = None

    def __init__(self):
        self.result = True
        self.message = 'success'

    def ok(self, message, result=True):
        self.result = result
        self.message = message
