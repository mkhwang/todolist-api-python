from werkzeug.exceptions import BadRequest


class CommonException(Exception):
    def __init__(self, param_name, request_obj):
        self.request_obj = request_obj
        self.param_name = param_name
        e = BadRequest()
        e.data = {'message': self.param_name,
                  'request_data': self.request_obj,
                  'result': False}
        raise e
