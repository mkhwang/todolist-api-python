from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restplus import Namespace, fields, reqparse, Resource

from app.api.core.response import common_response_field, CommonResponse
from app.service.task_service import TaskService

ns_task = Namespace('업무', description='업무관리 기능')

task_parser = reqparse.RequestParser(bundle_errors=True)
task_parser.add_argument('title', type=str, trim=True, required=True, location='json')
task_parser.add_argument('content', type=str, trim=True, required=True, location='json')
task_parser.add_argument('started_at', type=str, trim=True, required=True, location='json')
task_parser.add_argument('deadline_at', type=str, trim=True, required=True, location='json')
task_parser.add_argument('is_finished', type=bool, trim=True, required=True, location='json')

task_request = ns_task.model('TaskAddRequest', {
    'title': fields.String,
    'content': fields.String,
    'started_at': fields.Date,
    'deadline_at': fields.Date,
    'is_finished': fields.Boolean
})

task_response = ns_task.model('Task', {
    'idx': fields.Integer,
    'title': fields.String,
    'content': fields.String,
    'started_at': fields.Date,
    'deadline_at': fields.Date,
    'is_finished': fields.Boolean,
    'created_at': fields.String
})

task_add_response = ns_task.clone('TaskResponse', common_response_field, {
    'data': fields.Nested(task_response)
})

task_list_response = ns_task.clone('TaskListResponse', common_response_field, {
    'data': fields.List(fields.Nested(task_response))
})

ns_task.add_model('CommonResponse', common_response_field)

task_service = TaskService()


class TaskResponse(CommonResponse):
    def __init__(self, data=None):
        super().__init__()
        self.data = data


@ns_task.route('')
class TaskAdd(Resource):
    @ns_task.expect(task_request)
    @jwt_required
    @ns_task.doc(security='JWT', description='업무 등록')
    @ns_task.marshal_with(task_add_response)
    def post(self):
        """
        업무 등록
        """
        identity = get_jwt_identity()
        task_data = task_parser.parse_args()
        print(identity)
        print(task_data)
        task_data['user_idx'] = identity['id']
        return TaskResponse(task_service.add_task(task_data)), 201

    @jwt_required
    @ns_task.doc(security='JWT', description='사용자 업무 가져오기')
    @ns_task.marshal_with(task_list_response)
    def get(self):
        """
        사용자 업무 가져오기
        """
        identity = get_jwt_identity()
        return TaskResponse(task_service.get_user_tasks(identity['id'])), 200


@ns_task.route('/<int:task_idx>', endpoint='tasks')
class TaskModify(Resource):
    @ns_task.doc(security='JWT', description='업무 수정')
    @ns_task.response(204, 'Modify Success')
    @jwt_required
    def put(self, task_idx):
        """
        업무 수정
        """
        task_data = task_parser.parse_args()
        task_data['idx'] = task_idx
        task_service.modify_task(task_data)
        return None, 204

    @ns_task.doc(security='JWT', description='업무 아이디를 이용하여 업무 삭제')
    @ns_task.response(204, 'Delete Success')
    @jwt_required
    def delete(self, task_idx):
        """
        업무 삭제
        """
        task_service.delete_task(task_idx)
        return None, 204

    @ns_task.doc(security='JWT', description='업무 아이디를 이용하여 업무 가져오기')
    @jwt_required
    @ns_task.marshal_with(task_add_response)
    def get(self, task_idx):
        """
        업무 가져오기
        """
        return TaskResponse(task_service.get_task_by_id(task_idx)), 200


@ns_task.route('/<int:task_idx>/complete')
class TaskComplete(Resource):
    @ns_task.doc(security='JWT', description='업무 아이디를 이용하여 업무 완료/미완료 처리')
    @ns_task.response(204, 'Modify Success')
    @jwt_required
    def put(self, task_idx):
        """
        업무 완료/미완료 처리(toggle)
        """
        identity = get_jwt_identity()
        task_service.toggle_task(task_idx, identity['id'])
        return None, 204
