from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restplus import Namespace, Resource, fields, reqparse

from app.service.auth_service import AuthService
from app.api.core.response import CommonResponse, common_response_field
from app.domain.user.user import UserType

ns_me = Namespace('내 정보', description='내 정보 관리')

me_modify_parser = reqparse.RequestParser(bundle_errors=True)
me_modify_parser.add_argument('name', type=str, trim=True, required=False, location='json')
me_modify_parser.add_argument('password', type=str, trim=True, required=False, location='json')


auth_service = AuthService()

me_model = ns_me.model('User', {
    'idx': fields.Integer,
    'userId': fields.String(attribute='user_id'),
    'name': fields.String,
    'password': fields.String,
    'role': fields.String(enum=UserType._member_names_),
    'createdAt': fields.String(attribute='created_at'),
    'loginAt': fields.String(attribute='login_at')
})

me_modify_model = ns_me.model('ModifyUser', {
    'name': fields.String,
    'password': fields.String
})

me_response = ns_me.clone('LoginResponse', common_response_field, {
    'data': fields.Nested(me_model)
})


class MeResponse(CommonResponse):
    def __init__(self, data=None):
        super().__init__()
        self.data = data


@ns_me.route('')
class MeController(Resource):
    @jwt_required
    @ns_me.doc(security='JWT', description='내 정보 가져오기')
    @ns_me.marshal_with(me_response)
    def get(self):
        """
        내 정보 가져오기
        """
        identity = get_jwt_identity()
        return MeResponse(auth_service.find_user_by_id(identity['id'])), 200

    @jwt_required
    @ns_me.doc(security='JWT', description='이름 or password 수정')
    @ns_me.expect(me_modify_model)
    def put(self):
        """
        내 정보 수정
        """
        identity = get_jwt_identity()
        modify_data = me_modify_parser.parse_args()
        modify_data['idx'] = identity['id']
        auth_service.modify_user(modify_data)
        return None, 204

    @jwt_required
    @ns_me.doc(security='JWT', description='회원탈퇴')
    def delete(self):
        """
        회원 탈퇴
        """
        identity = get_jwt_identity()
        auth_service.delete_user(identity['id'])
        return None, 204
