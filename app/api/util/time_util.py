from datetime import datetime


def get_current():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')