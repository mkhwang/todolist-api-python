import traceback

from flask_bcrypt import Bcrypt
from flask_jwt_extended import create_refresh_token, create_access_token, jwt_required, get_jwt_identity
from flask_restplus import Resource, reqparse, fields, Namespace

from app.service.auth_service import AuthService
from app.api.core.exception.common_exception import CommonException
from app.api.core.response import common_response_field, CommonResponse
from app.repository.user_repository import UserRepository

ns_auth = Namespace('인증', description='인증 관련')

login_parser = reqparse.RequestParser(bundle_errors=True)
login_parser.add_argument('userId', type=str, trim=True, required=True, location='json')
login_parser.add_argument('password', type=str, trim=True, required=True, location='json')

join_parser = reqparse.RequestParser(bundle_errors=True)
join_parser.add_argument('userId', type=str, trim=True, required=True, location='json')
join_parser.add_argument('password', type=str, trim=True, required=True, location='json')
join_parser.add_argument('name', type=str, trim=True, required=True, location='json')

login_request = ns_auth.model('LoginRequest', {
    'userId': fields.String,
    'password': fields.String
})

join_request = ns_auth.clone('JoinRequest', login_request, {
    'name': fields.String
})

token_response = ns_auth.model("Token", {
    'accessToken': fields.String,
    'refreshToken': fields.String
})

login_response = ns_auth.clone('LoginResponse', common_response_field, {
    'data': fields.Nested(token_response)
})

check_user_id_response = ns_auth.clone('CheckUserIdResponse', common_response_field, {
    'data': fields.Boolean
})

ns_auth.add_model('CommonResponse', common_response_field)

user_repository = UserRepository()
bcrypt = Bcrypt()

auth_service = AuthService()


class Token:
    access_token = ''
    refresh_token = ''

    def __init__(self, user):
        resource = {
            'id': user['idx'],
            'role': user['role']
        }
        self.accessToken = create_access_token(resource)
        self.refreshToken = create_refresh_token(resource)


class LoginResult(CommonResponse):
    data = None

    def __init__(self, user):
        super().__init__()
        if user is not None:
            self.data = Token(user)
        else:
            self.data = None
            self.message = 'Invalid ID or Password'
            self.result = False


class CheckUserIdResponse(CommonResponse):
    def __init__(self, data):
        super().__init__()
        if data is True:
            self.data = False
            self.result = False
            self.message = 'duplicated'
        else:
            self.data = True
            self.result = True
            self.message = 'success'


@ns_auth.route('/login')
class Login(Resource):
    @ns_auth.expect(login_request)
    @ns_auth.marshal_with(login_response)
    @ns_auth.doc(security=None, description='ID/PW 로 로그인 후 ACCESS TOKEN 을 받음')
    def post(self):
        """
        로그인
        """
        login_data = login_parser.parse_args()
        find_user = user_repository.get_user_by_user_id(login_data['userId'])
        if find_user is not None:
            find_user = find_user.serialize()
            auth_service.login_user(find_user['idx'])
        else:
            raise CommonException('ID', login_data)

        if bcrypt.check_password_hash(find_user['password'], login_data['password']) is False:
            raise CommonException('Password', login_data)

        return LoginResult(find_user), 200


@ns_auth.route('/join')
class Join(Resource):
    @ns_auth.expect(join_request)
    @ns_auth.marshal_with(common_response_field)
    @ns_auth.doc(security=None, description='ID & PASSWORD & NAME 을 이용하여 회원가입')
    def post(self):
        """
        회원가입
        """
        join_data = join_parser.parse_args()
        message = 'Join Success'
        result = True
        try:
            password = bcrypt.generate_password_hash(join_data['password'])
            join_data['password'] = password
            join_data['user_id'] = join_data['userId']
            join_data.pop('userId')
            user_repository.add_user(join_data)
        except Exception as e:
            traceback.print_exc()
            message = str(e)
            result = False

        return CommonResponse().ok(message, result), 201


@ns_auth.route('/logout')
class LogOut(Resource):
    @jwt_required
    @ns_auth.doc(security='JWT', description='로그아웃')
    @ns_auth.marshal_with(common_response_field)
    def post(self):
        """
        로그아웃
        """
        identity = get_jwt_identity()
        auth_service.logout_user(identity['id'])
        return CommonResponse(), 200


@ns_auth.route('/users/<string:user_id>')
class CheckUser(Resource):
    @ns_auth.doc(security=None, description='아이디 중복 체크')
    @ns_auth.marshal_with(check_user_id_response)
    def get(self, user_id):
        """
        아이디 중복 체크
        """
        exist_user = user_repository.is_duplicated_user_id(user_id)
        return CheckUserIdResponse(exist_user), 200
