import datetime

from flask import Flask, jsonify
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_jwt_extended.exceptions import NoAuthorizationError, InvalidHeaderError, JWTDecodeError, WrongTokenError
from flask_orator import Orator
from flask_restplus import Api
from jwt import ExpiredSignatureError, InvalidTokenError

from app.api.admin.admin_controller import ns_admin
from app.api.auth.auth_controller import ns_auth
from app.api.me.me_controller import ns_me
from app.api.task.task_controller import ns_task

database = Orator()
todo_list_bcrypt = Bcrypt()


def create_app():
    app = Flask(__name__)

    # JWT Config
    app.config['JWT_SECRET_KEY'] = 'todo-list-hyper-secret'
    app.config['JWT_TOKEN_LOCATION'] = 'headers'
    app.config['JWT_ALGORITHM'] = 'HS256'
    app.config['JWT_HEADER_NAME'] = 'X-TodoList-Authorization'
    app.config['JWT_HEADER_TYPE'] = 'Bearer'
    app.config['JWT_CSRF_METHODS'] = []
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)
    app.config['JWT_REFRESH_TOKEN_EXPIRES'] = datetime.timedelta(days=7)

    # Orator Config
    app.config['ORATOR_DATABASES'] = {
        'default': 'todo-list',
        'todo-list': {
            'driver': 'mysql',
            'host': 'zeromarketdbinstance.crrrtitosdk7.ap-northeast-2.rds.amazonaws.com',
            'port': 33060,
            'database': 'todolist_python',
            'user': 'todolist_python',
            'password': 'todolist_python!',
            'log_queries': True
        }
    }

    app.config['RESTPLUS_MASK_SWAGGER'] = False

    cors = CORS(app, resources={r"/*": {"origins": "*"}})
    cors.init_app(app)

    database.init_app(app)
    # Password bcrypt
    todo_list_bcrypt.init_app(app)

    errors = {
        'CustomError': {
            'message': 'custom error',
            'status': 404
        }
    }

    authorization = {
        'JWT': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'X-TodoList-Authorization'
        }
    }

    api = Api(app,
              errors=errors,
              version='0.0',
              title='Todo-List(Python-Flask)',
              description='Python-Flask를 이용한 REST API',
              doc='/documentation',
              contact='hmk6264@gmail.com',
              authorizations=authorization,
              security='apiKey')

    api.namespaces.clear()
    api.add_namespace(ns_auth, path='/auth')
    api.add_namespace(ns_admin, path='/admin')
    api.add_namespace(ns_task, path='/tasks')
    api.add_namespace(ns_me, path='/me')

    jwt = JWTManager(app)
    jwt.init_app(app)

    @jwt.unauthorized_loader
    @jwt.invalid_token_loader
    def my_expired_token_callback():
        return jsonify({
            'status': 401,
            'sub_status': 42,
            'msg': 'The token has expired'
        }), 401

    @api.errorhandler(NoAuthorizationError)
    def handle_auth_error(e):
        return {'message': str(e), 'result': False}, 401

    @api.errorhandler(ExpiredSignatureError)
    def handle_expired_error(e):
        return {'message': 'Token has expired', 'result': False}, 401

    @api.errorhandler(InvalidHeaderError)
    def handle_invalid_header_error(e):
        return {'message': str(e), 'result': False}, 422

    @api.errorhandler(InvalidTokenError)
    def handle_invalid_token_error(e):
        return {'message': 'Invalid Token', 'result': False}, 422

    @api.errorhandler(JWTDecodeError)
    def handle_jwt_decode_error(e):
        return {'message': str(e), 'result': False}, 422

    @api.errorhandler(WrongTokenError)
    def handle_wrong_token_error(e):
        return {'message': str(e), 'result': False}, 422

    return app
