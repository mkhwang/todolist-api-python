from app.api.util.time_util import get_current
from app.domain.user.user import User


class UserRepository:
    def __init__(self):
        pass

    def add_user(self, user):
        User.insert(user)

    def is_duplicated_user_id(self, user_id):
        return User.where('user_id', user_id).first() is not None

    def get_user_by_user_id(self, user_id):
        return User.where('user_id', user_id).first()

    def login_user(self, user_idx):
        User.where('idx', user_idx).update({'login_at': get_current()})

    def logout_user(self, user_idx):
        User.where('idx', user_idx).update({'logout_at': get_current()})

    def modify_user(self, user):
        User.where('idx', user['idx']).update(user)

    def delete_user(self, user_idx):
        User.where('idx', user_idx).delete()

    def get_user_by_idx(self, user_idx):
        return User.where('idx', user_idx).get().first()

    def get_user_task(self, user_idx):
        return User.where('idx', user_idx).has('tasks').get().serialize()

    def get_all_users(self):
        return User.all()