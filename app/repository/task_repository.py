from app.api.util.time_util import get_current
from app.domain.task.task import Task


class TaskRepository:
    def __init__(self):
        pass

    def add_task(self, task):
        with Task.get_connection(Task).transaction():
            task = Task.insert(task)
        return task

    def modify_task(self, task):
        with Task.get_connection(Task).transaction():
            task['updated_at'] = get_current()
            Task.where('idx', task['idx']).update(task)

    def delete_task(self, task_idx):
        with Task.get_connection(Task).transaction():
            Task.where('idx', task_idx).delete()

    def get_task_by_id(self, task_idx):
        return Task.where('idx', task_idx).first()

    def finish_task(self, task_idx):
        with Task.get_connection(Task).transaction():
            find_task = Task.where('idx', task_idx).first().serialize()
            print(find_task)
            Task.where('idx', task_idx).update({'is_finished': not find_task['is_finished'],
                                                'updated_at': get_current()})

    def get_tasks_by_user_id(self, user_idx):
        return Task.where('user_idx', user_idx).order_by('created_at', 'desc').get()

    def get_task_by_id_and_user_idx(self, task_idx, user_idx):
        return Task.where('idx', task_idx).where('user_idx', user_idx).first()
