import enum

from orator import Model
from orator.orm import has_many

from app.domain.task.task import Task


class User(Model):
    __table__ = 'user'
    __fillable__ = ['idx', 'user_id', 'password', 'name', 'role', 'created_at', 'updated_at', 'login_at', 'logout_at',
                    'deleted_at']
    __columns__ = ['idx', 'user_id', 'password', 'name', 'role', 'created_at', 'updated_at', 'login_at', 'logout_at',
                   'deleted_at']
    __timestamps__ = False
    __casts__ = {
        'created_at': 'str',
        'updated_at': 'str',
        'login_at': 'str',
        'deleted_at': 'str'
    }
    __primary_key__ = ['idx']

    def __repr__(self):
        return '<user %r %r>' % (self.idx, self.user_id)

    def get_date_format(self):
        return '%Y-%m-%d %H:%M:%S'

    @has_many(local_key='idx', foreign_key='user_idx')
    def tasks(self):
        return Task


class UserType(enum.Enum):
    ROLE_ADMIN = 'ROLE_ADMIN',
    ROLE_USER = 'ROLE_USER'
