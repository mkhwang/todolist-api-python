from orator import Model
from orator.orm import belongs_to


class Task(Model):
    __table__ = 'task'
    __fillable__ = ['idx', 'user_idx', 'title', 'content', 'is_finished', 'created_at', 'updated_at', 'deleted_at',
                    'started_at', 'deadline_at']
    __columns__ = ['idx', 'user_idx', 'title', 'content', 'is_finished', 'created_at', 'updated_at', 'deleted_at',
                   'started_at', 'deadline_at']
    __timestamps__ = False
    __casts__ = {
        'created_at': 'str',
        'updated_at': 'str',
        'deleted_at': 'str',
        'started_at': 'str',
        'deadline_at': 'str'
    }
    __primary_key__ = ['idx']

    def __repr__(self):
        return '<user %r %r>' % (self.idx, self.user_id)

    def get_date_format(self):
        return '%Y-%m-%d %H:%M:%S'

    @belongs_to(foreign_key='user_idx')
    def user(self):
        return User
