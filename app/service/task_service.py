from app.api.core.exception.common_exception import CommonException
from app.domain.task.task import Task
from app.repository.task_repository import TaskRepository


class TaskService:
    def __init__(self):
        self.task_repository = TaskRepository()

    def add_task(self, task):
        with Task.get_connection(Task).transaction():
            task = self.task_repository.add_task(task)
        return task

    def modify_task(self, task):
        with Task.get_connection(Task).transaction():
            self.task_repository.modify_task(task)

    def delete_task(self, task_idx):
        with Task.get_connection(Task).transaction():
            self.task_repository.delete_task(task_idx)

    def toggle_task(self, task_idx, user_idx):
        if self.is_user_task(task_idx, user_idx):
            raise CommonException('Not your Task', {'task_id': task_idx})
        with Task.get_connection(Task).transaction():
            self.task_repository.finish_task(task_idx)

    def get_task_by_id(self, task_idx):
        return self.task_repository.get_task_by_id(task_idx)

    def get_user_tasks(self, user_idx):
        return self.task_repository.get_tasks_by_user_id(user_idx).serialize()

    def is_user_task(self, task_idx, user_idx):
        return self.task_repository.get_task_by_id_and_user_idx(task_idx, user_idx) is None