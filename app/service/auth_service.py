from app.domain.user.user import User
from app.repository.user_repository import UserRepository


class AuthService:
    def __init__(self):
        self.user_repository = UserRepository()

    def add_user(self, user):
        with User.get_connection(User).transaction():
            self.user_repository.add_user(user)
        return user

    def modify_user(self, user):
        with User.get_connection(User).transaction():
            self.user_repository.modify_user(user)

    def delete_user(self, user_idx):
        with User.get_connection(User).transaction():
            self.user_repository.delete_user(user_idx)

    def login_user(self, user_idx):
        with User.get_connection(User).transaction():
            self.user_repository.login_user(user_idx)
            print(user_idx)

    def logout_user(self, user_idx):
        with User.get_connection(User).transaction():
            self.user_repository.logout_user(user_idx)

    def find_user_by_id(self, user_idx):
        return self.user_repository.get_user_by_idx(user_idx).serialize()

    def find_all_users(self):
        return self.user_repository.get_all_users().serialize()